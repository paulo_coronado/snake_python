## Propósito
Proyecto de juego de snake para el desarrollo de competencias de pensamiento computacional y explorar algunos conceptos básicos del paradigma orientado a objetos con lenguaje de programación Python. 

## Nivel de Dificultad (0-10)
5

## Conceptos abordados

### Pensamiento computacional
* Descomposición
* Pensamiento algorítmico
* Abstracción.

### Paradigma de programación orientado a objetos
* Clase
  -  Atributos
  -  Métodos
* objetos
* Llamada a métodos
* Cohesión
* Inyección de dependencias
### Python
* class
  - def
  - \_\_init\_\_
  - self
* List
    * insert()
    * pop()
* range()
* random
* string.format()
* for
* while
* break
* if
* random.randint()
* == (igual a)
* != (diferente a)
* Biblioteca Pygame
  * init()
  * quit()
  * time
      * Clock
          * tick()
      * wait()
  * display
      * set_mode()
      * fill()
      * flip()
      * blit()
  * key
      * get_pressed()
  * event
      * get()
  * draw
      * rect()
  * Rect
      * colliderect()
  * font
      * SysFont
          * render()
      
      
## Nota

Recurso Educativo Abierto para el curso de programación de computadores para el desarrollo del pensamiento computacional en estudiantes de ingeniería. Este código fuente hace parte de una sesión de live coding que se encuentra en el canal de youtube de Paulo César Coronado (paulocoronado@udistrital.edu.co)
