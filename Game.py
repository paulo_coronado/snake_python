from tkinter.messagebox import NO
import pygame
from Gamer import Gamer
from Snake import Snake
from Food import Food

class SnakeGame:
    def __init__(self, width, height, fps):
        self.width = width
        self.height = height
        self.fps = fps
        self.screen = pygame.display.set_mode((self.width, self.height), 0, 32)
        self.clock = pygame.time.Clock()
        self.mySnake = Snake()
        self.snakeHeadRect= None
        self.food = Food(0, 0)
        self.foodRect=None
        self.myGamer=Gamer()

    def run(self):
        pygame.init()
        while True:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return
            # Clear the screen
            self.screen.fill((255, 255, 255))
            
            # Draw the attributes of myGamer
            self.drawGamer()

            # Check the keys pressed
            self.checkKeys()

            # Draw the food
            self.drawFood()
            
            # Check if the snake eats the food
            self.checkEat();
                        
            # Move the snake
            self.mySnake.move()
            
            #Check if the snake dies
            self.checkDie()

            # Draw mySnake
            for cell in self.mySnake.body:
                pygame.draw.rect(self.screen, (0, 0, 0), (cell[0], cell[1], 10, 10))

            
            pygame.display.flip()
            self.clock.tick(self.fps)
            
    def checkKeys(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP]:
            self.mySnake.direction = "UP"
        elif keys[pygame.K_DOWN]:
            self.mySnake.direction = "DOWN"
        elif keys[pygame.K_LEFT]:
            self.mySnake.direction = "LEFT"
        elif keys[pygame.K_RIGHT]:
            self.mySnake.direction = "RIGHT"
        elif keys[pygame.K_q]:
            self.fps += 5
        elif keys[pygame.K_a]:
            self.fps -= 5
    
    def checkDie(self):
        
        if self.mySnake.die(self.width, self.height):
            #Clear the screen
            self.screen.fill((255, 255, 255))
            #Show a message
            font = pygame.font.SysFont("Arial", 15)
            scoreText = font.render("Game Over", True, (255,0, 0))
            self.screen.blit(scoreText, (self.width/2-70, self.height/2-80))
            
            # Show the score
            font = pygame.font.SysFont("Arial", 20)
            scoreText = font.render("Score: " + str(self.myGamer.score), True, (0, 0, 0))
            self.screen.blit(scoreText, (self.width/2-50, self.height/2-50))    
            pygame.display.flip()   
            # Wait for 3 seconds
            pygame.time.wait(3000)   
            #Quit the game
            pygame.quit()
                    
            return
            
    
    def drawFood(self):
        if self.food.status == "eaten":
            while True:
                (x, y) = self.food.putFood(self.width, self.height)
                if not (x, y) in self.mySnake.body:
                    break
            self.food.status = "alive"
        self.foodRect=pygame.Rect(self.food.x, self.food.y, self.food.size, self.food.size)
        pygame.draw.rect(self.screen, self.food.color, self.foodRect)
    
    def checkEat(self):
        self.snakeHeadRect=pygame.Rect(self.mySnake.body[0][0], self.mySnake.body[0][1], 10, 10)
        if pygame.Rect.colliderect(self.foodRect, self.snakeHeadRect):
            self.food.status = "eaten"
            self.mySnake.eat()
            self.myGamer.score += 1
    
    def drawGamer(self):
        # Draw the score
        font = pygame.font.SysFont("Arial", 20)
        scoreText = font.render("Score: " + str(self.myGamer.score), True, (0, 0, 0))
        self.screen.blit(scoreText, (0, 0))
        
        # Draw the name
        nameText = font.render("Name: " + self.myGamer.name, True, (0, 0, 0))
        self.screen.blit(nameText, (0, 20))


mySnakeGame = SnakeGame(400, 400, 60)
mySnakeGame.run()
