
import random


class Food:
    
    def __init__(self, x, y):
        self.x = 0
        self.y = 0
        self.color = (255,0,0)
        self.size=10      
        self.status="eaten"  
    
    def putFood(self,x_limit,y_limit):        
        self.x = random.randint(0, x_limit)
        self.y = random.randint(0, y_limit)
        return (self.x, self.y)