#Create a pygame window
import pygame


def set_direction():
    #If the snake goes off the screen, it will appear on the other side
    if x > screen.get_width(): x = 0
    if x < 0: x = screen.get_width()
    if y > screen.get_height(): y = 0
    if y < 0: y = screen.get_height()

pygame.init()
screen = pygame.display.set_mode((640, 480))
pygame.display.set_caption("My First Game") 
# Game loop
running = True

#Set default coordinates
x = screen.get_width() / 2
y=  screen.get_height() / 2

direction= "up"

while running:
    #Limit the frame rate to 60 FPS
    clock = pygame.time.Clock()
    clock.tick(60)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False    
    
    # Fill the background with white
    screen.fill((255, 255, 255))
    
    #Capture key presses
    pressed = pygame.key.get_pressed()

    #Move the snake
    if pressed[pygame.K_UP]: 
        y -= 3 
        direction = "up"
            
    if pressed[pygame.K_DOWN]: 
        y += 3
        direction = "down"
    if pressed[pygame.K_LEFT]: 
        x -= 3
        direction = "left"
    if pressed[pygame.K_RIGHT]: 
        x += 3
        direction = "right"
    
    #If not key is pressed, the snake will move in the last direction
    if pressed[pygame.K_UP] == False and pressed[pygame.K_DOWN] == False and pressed[pygame.K_LEFT] == False and pressed[pygame.K_RIGHT] == False:
        if direction == "up": y -= 3
        if direction == "down": y += 3
        if direction == "left": x -= 3
        if direction == "right": x += 3
        
    #If the snake goes off the screen, it will change direction and increase size
    if x > screen.get_width(): 
        direction = "left"
    if x < 0: 
        direction = "right"
        
    if y > screen.get_height(): 
        direction = "up"
    if y < 0: 
        direction = "down"
    
    # Draw a solid black square in the center of size 10x10
    pygame.draw.rect(screen, (0, 0, 0), (x, y, 10, 10))
    
    #refresh the screen
    pygame.display.flip()   
        

 
pygame.quit()               
    