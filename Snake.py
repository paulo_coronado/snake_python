

class Snake:
    
    def __init__(self):
        # initialize the snake with a body of 1        
        
        self.cell_size = 1
        self.direction = "RIGHT"
        self.last_cell = (0,0)
        
        self.body = [(self.cell_size*2,0),(self.cell_size,0),(0,0)]
        
    def move(self):        
        #Check the coordinates of the first cell
        x, y = self.body[0]
        
        #Verify the direction and update the coordinates
        x,y = self.updateCoordinates(x, y)       
            
        # Add a new head to the snake
        self.body.insert(0, (x,y))
        # Remove the tail
        self.last_cell=self.body.pop()
        
        
    def updateCoordinates(self, x, y):
        if self.direction == "UP":
            y -= 1
        elif self.direction == "DOWN":
            y += 1
        elif self.direction == "LEFT":
            x -= 1
        elif self.direction == "RIGHT":     
            x += 1
        return (x,y)
    
    def eat(self):
        #Add 10 cells to the snake
        (x,y) = self.body[0]
        for i in range(10):
            self.body.insert(0, (x,y))
            x,y=self.updateCoordinates(x, y)
            
    
    def die(self,screen_width, screen_height):
        #Check if the snake is out of the screen
        x, y = self.body[0]
        if x < 0 or x > screen_width or y < 0 or y > screen_height:
            return True
        #Check if the snake bites itself
        if self.body[0] in self.body[1:]:
            return True
        return False
        
        

    
    
        
        
